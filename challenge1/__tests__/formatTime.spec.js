const format = require('../src/formatTime');
let formatTime = new format.Time();

describe("formatTime function", () => {

    test("formatTime(0) should return none", () => {
        let time = formatTime.asText(0)
        expect(time).toEqual('none');
    });
    test("formatTime(1) should return 1 second", () => {
        let time = formatTime.asText(1)
        expect(time).toEqual('1 second');
    });
    test("formatTime(2) should return 2 seconds", () => {
        let time = formatTime.asText(2)
        expect(time).toEqual('2 seconds');
    });
    test("formatTime(60) should return 1 minute", () => {
        let time = formatTime.asText(60)
        expect(time).toEqual('1 minute');
    });
    test("formatTime(62) should return 1 minute and 2 seconds", () => {
        let time = formatTime.asText(62)
        expect(time).toEqual('1 minute and 2 seconds');
    });
    test("formatTime(3600) should return 1 hour", () => {
        let time = formatTime.asText(3600)
        expect(time).toEqual('1 hour');
    });
    test("formatTime(3660) should return 1 hour and 1 minute", () => {
        let time = formatTime.asText(3660)
        expect(time).toEqual('1 hour and 1 minute');
    });
    test("formatTime(3603) should return 1 hour and 3 seconds", () => {
        let time = formatTime.asText(3603)
        expect(time).toEqual('1 hour and 3 seconds');
    });
    test("formatTime(3663) should return 1 hour, 1 minutes and 3 seconds", () => {
        let time = formatTime.asText(3663)
        expect(time).toEqual('1 hour, 1 minute and 3 seconds');
    });
    test("formatTime(86400) should return 1 day", () => {
        let time = formatTime.asText(86400)
        expect(time).toEqual('1 day');
    });
    test("formatTime(604800) should return 1 week", () => {
        let time = formatTime.asText(604800)
        expect(time).toEqual('1 week');
    });
    test("formatTime(31536000) should return 1 year", () => {
        let time = formatTime.asText(31536000)
        expect(time).toEqual('1 year');
    });
    test("formatTime(31539663) should return 1 year, 1 hour, 1 minute and 3 seconds", () => {
        let time = formatTime.asText(31539663)
        expect(time).toEqual('1 year, 1 hour, 1 minute and 3 seconds');
    });
  });