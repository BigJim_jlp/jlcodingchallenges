
function Time() {
    
};

Time.prototype.asText = function(seconds) {

    if (seconds == 0) {return `none`;}

    numOfYears = Math.floor(seconds/31536000);
    yearsInSecs = numOfYears * 31536000;

    numOfWeeks = Math.floor((seconds - yearsInSecs)/604800);
    weeksInSecs = numOfWeeks * 604800;

    numOfDays = Math.floor((seconds - yearsInSecs - weeksInSecs)/86400);
    daysInSecs = numOfDays * 86400;

    numOfHours = Math.floor((seconds - yearsInSecs - weeksInSecs - daysInSecs)/3600);
    hoursInSecs = numOfHours * 3600;

    numOfMinutes = Math.floor((seconds - yearsInSecs - weeksInSecs - daysInSecs - hoursInSecs)/60);
    minsInSecs = numOfMinutes * 60;

    numOfSeconds = seconds - yearsInSecs - weeksInSecs - daysInSecs - hoursInSecs - minsInSecs;

    return this.createString(numOfYears, numOfWeeks, numOfDays, numOfHours, numOfMinutes, numOfSeconds);

};

Time.prototype.convertToText = function(timeType, timeNumber, alreadyStarted){
    let filler = "";
    if (alreadyStarted){
        filler = ", "
    }
    if (timeNumber === 1){
        return `${filler}${timeNumber} ${timeType}`
    }
    if (timeNumber > 1){
        return `${filler}${timeNumber} ${timeType}s`
    }

    return "";
}

Time.prototype.createString = function(years, weeks, days, hours, minutes, seconds) {
    
    let returnString = '';
    returnString += this.convertToText("year", years, returnString.length>0);
    returnString += this.convertToText("week", weeks, returnString.length>0);
    returnString += this.convertToText("day", days, returnString.length>0);
    returnString += this.convertToText("hour", hours, returnString.length>0);
    returnString += this.convertToText("minute", minutes, returnString.length>0);
    returnString += this.convertToText("second", seconds, returnString.length>0);
    
    lastComma = returnString.lastIndexOf(',');
    if (lastComma === -1) { return returnString };
    let response = '';
    response = `${returnString.substring(0, lastComma)} and${returnString.substring(lastComma +1, returnString.length)}`;

    return response;
}

exports.Time = Time;